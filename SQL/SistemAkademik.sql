
/*Nomor 1 Tampilkan data NM_FAKULTAS, NM_JURUSAN*/
SELECT "NM_FAKULTAS", "NM_JURUSAN" FROM "Fakultas", "Jurusan";

CREATE VIEW "SOAL01" AS SELECT "NM_FAKULTAS", "NM_JURUSAN" FROM "Fakultas", "Jurusan";


/*2. Tampilkan data NM_FAKULTAS, NM_JURUSAN yang NM_FAKULTAS mengandung nama ‘fi’*/
SELECT t1."NM_FAKULTAS", t2."NM_JURUSAN", t2."KD_JURUSAN"
	FROM "Fakultas" t1
	INNER JOIN "Jurusan" t2 ON t1."KD_FAKULTAS"=t2."KD_FAKULTAS" 
	WHERE t1."NM_FAKULTAS" like '%AK%';

CREATE VIEW "SOAL2" AS SELECT t1."NM_FAKULTAS", t2."NM_JURUSAN", t2."KD_JURUSAN"
	FROM "Fakultas" t1
	INNER JOIN "Jurusan" t2 ON t1."KD_FAKULTAS"=t2."KD_FAKULTAS" 
	WHERE t1."NM_FAKULTAS" like '%AK%';
	
SELECT t02."NM_FAKULTAS", t02."NM_JURUSAN", t02."KD_JURUSAN"
	FROM "SOAL2" t02
	WHERE t02."NM_FAKULTAS" like '%AK%';
	
/*3.Tampilkan data NM_FAKULTAS, NM_JURUSAN yang NM_JURUSAN mengandung nama ‘ti’*/
SELECT t1."NM_FAKULTAS", t2."NM_JURUSAN", t2."KD_JURUSAN"
	FROM "Fakultas" t1 
	INNER JOIN "Jurusan" t2 ON t1."KD_FAKULTAS"=t2."KD_FAKULTAS" 
	WHERE t2."NM_JURUSAN" like '%ti%';
	
CREATE VIEW "SOAL3" AS SELECT t1."NM_FAKULTAS", t2."NM_JURUSAN", t2."KD_JURUSAN"
	FROM "Fakultas" t1 
	INNER JOIN "Jurusan" t2 ON t1."KD_FAKULTAS"=t2."KD_FAKULTAS" 
	WHERE t2."NM_JURUSAN" like '%ti%';
	
SELECT t3."NM_FAKULTAS", t3."NM_JURUSAN", t3."KD_JURUSAN"
FROM "SOAL3" t3 WHERE t3."NM_JURUSAN" like '%ti%';

/*4. Tampilkan data NM_FAKULTAS, JML_JURUSAN*/
SELECT t1."NM_FAKULTAS", COUNT(t2."KD_FAKULTAS") AS "JML_JURUSAN"
FROM "Fakultas" t1
LEFT JOIN "Jurusan" t2 ON t1."KD_FAKULTAS"=t2."KD_FAKULTAS"
GROUP BY t1."NM_FAKULTAS";

CREATE VIEW "SOAL4" AS SELECT t1."NM_FAKULTAS", COUNT(t2."KD_FAKULTAS") AS "JML_JURUSAN"
FROM "Fakultas" t1
LEFT JOIN "Jurusan" t2 ON t1."KD_FAKULTAS"=t2."KD_FAKULTAS"
GROUP BY t1."NM_FAKULTAS";
 

/*5.Tamilkan data NM_FAKULTAS, JML_JURUSAN yang JML_JURUSAN lebih dari 1*/
SELECT t1."NM_FAKULTAS", COUNT(t2."KD_FAKULTAS") AS "JML_JURUSAN"
FROM "Fakultas" t1
LEFT JOIN "Jurusan" t2 ON t1."KD_FAKULTAS"=t2."KD_FAKULTAS"
GROUP BY t1."NM_FAKULTAS"
HAVING COUNT(t2."KD_FAKULTAS")>1;


CREATE VIEW "SOAL5" AS SELECT t1."NM_FAKULTAS", COUNT(t2."KD_FAKULTAS") AS "JML_JURUSAN"
FROM "Fakultas" t1
LEFT JOIN "Jurusan" t2 ON t1."KD_FAKULTAS"=t2."KD_FAKULTAS"
GROUP BY t1."NM_FAKULTAS";

SELECT t5."NM_FAKULTAS", t5."JML_JURUSAN"
FROM "SOAL5" t5
WHERE t5."JML_JURUSAN">1


/*6.Tampilkan data NM_FAKULTAS, JML_JURUSAN yang TIDAK memiliki Jurusan*/
SELECT t1."NM_FAKULTAS", COUNT(t2."KD_FAKULTAS") AS "JML_JURUSAN"
FROM "Fakultas" t1
LEFT JOIN "Jurusan" t2 ON t1."KD_FAKULTAS"=t2."KD_FAKULTAS"
GROUP BY t1."NM_FAKULTAS"
HAVING COUNT(t2."KD_FAKULTAS")=0;

/*PAKAI CREATE VIEW*/
CREATE VIEW "SOAL6" ASSELECT t1."NM_FAKULTAS", COUNT(t2."KD_FAKULTAS") AS "JML_JURUSAN"
FROM "Fakultas" t1
LEFT JOIN "Jurusan" t2 ON t1."KD_FAKULTAS"=t2."KD_FAKULTAS"
GROUP BY t1."NM_FAKULTAS";

SELECT t6."NM_FAKULTAS", t6."JML_JURUSAN"
FROM "SOAL6" t6
WHERE t6."JML_JURUSAN"=0;

/*7.Tampilkan data NM_MK, NM_JURUSAN, NM_FAKULTAS*/
SELECT t1."NM_FAKULTAS" , t2."NM_JURUSAN", t3."NM_MK"
FROM "Fakultas" t1 
JOIN "Jurusan" t2 ON t1."KD_FAKULTAS"=t2."KD_FAKULTAS"
JOIN "Matakuliah" t3 ON t2."KD_JURUSAN"=t3."KD_JURUSAN";

CREATE VIEW "SOAL7" AS SELECT t1."NM_FAKULTAS" , t2."NM_JURUSAN", t3."NM_MK"
FROM "Fakultas" t1 
JOIN "Jurusan" t2 ON t1."KD_FAKULTAS"=t2."KD_FAKULTAS"
JOIN "Matakuliah" t3 ON t2."KD_JURUSAN"=t3."KD_JURUSAN";

/*8.Tampilkan data NM_JURUSAN, JML_MK*/
SELECT t1."NM_JURUSAN", COUNT(t2."KD_JURUSAN") AS "JML_MK"
FROM "Jurusan" t1 
LEFT JOIN "Matakuliah" t2 ON t1."KD_JURUSAN"=t2."KD_JURUSAN"
GROUP BY t1."NM_JURUSAN";

CREATE VIEW "SOAL8" AS  SELECT t1."NM_JURUSAN", COUNT(t2."KD_JURUSAN") AS "JML_MK"
FROM "Jurusan" t1
LEFT JOIN "Matakuliah" t2 ON t1."KD_JURUSAN"=t2."KD_JURUSAN"
GROUP BY t1."NM_JURUSAN";

/*9.Tampilkan data NM_JURUSAN, JML_MK yang SKS nya 2*/
SELECT t1."NM_JURUSAN", COUNT(t2."KD_JURUSAN") AS "JML_MK"
FROM "Jurusan" t1 
FULL JOIN "Matakuliah" t2 ON t1."KD_JURUSAN"=t2."KD_JURUSAN"
WHERE  t2."SKS"=2
GROUP BY "NM_JURUSAN";

CREATE VIEW "SOAL9" AS SELECT t1."NM_JURUSAN", COUNT(t2."KD_JURUSAN") AS "JML_MK"
FROM "Jurusan" t1 
FULL JOIN "Matakuliah" t2 ON t1."KD_JURUSAN"=t2."KD_JURUSAN"
WHERE  t2."SKS"=2
GROUP BY "NM_JURUSAN";

/*10. Tampilkan data NM_JURUSAN, JML_MK yang SKS nya 3*/
SELECT t1."NM_JURUSAN", COUNT(t2."KD_JURUSAN") AS "JML_MK"
FROM "Jurusan" t1 
JOIN "Matakuliah" t2 ON t1."KD_JURUSAN"=t2."KD_JURUSAN"
WHERE  t2."SKS"=3
GROUP BY "NM_JURUSAN";

CREATE VIEW "SOAL10" AS SELECT t1."NM_JURUSAN", COUNT(t2."KD_JURUSAN") AS "JML_MK"
FROM "Jurusan" t1 
FULL JOIN "Matakuliah" t2 ON t1."KD_JURUSAN"=t2."KD_JURUSAN"
WHERE  t2."SKS"=3
GROUP BY "NM_JURUSAN";

/*11. Tampilkan data NM_FAKULTAS, NM_JURUSAN yang TIDAK memiliki Matakuliah*/
SELECT t1."NM_FAKULTAS", t2."NM_JURUSAN"
FROM "Fakultas" t1
INNER JOIN "Jurusan" t2 ON t1."KD_FAKULTAS"=t2."KD_FAKULTAS"
LEFT JOIN "Matakuliah" t3 ON t2."KD_JURUSAN"=t3."KD_JURUSAN"
GROUP BY "NM_FAKULTAS","NM_JURUSAN"
HAVING COUNT(t3."NM_MK")=0;

CREATE VIEW "SOAL11" AS SELECT t1."NM_FAKULTAS", t2."NM_JURUSAN"
FROM "Fakultas" t1
INNER JOIN "Jurusan" t2 ON t1."KD_FAKULTAS"=t2."KD_FAKULTAS"
LEFT JOIN "Matakuliah" t3 ON t2."KD_JURUSAN"=t3."KD_JURUSAN"
GROUP BY "NM_FAKULTAS","NM_JURUSAN" HAVING COUNT(t11."NM_MK")=0;	
	

/*12	Tampilkan data NM_MK, NM_JURUSAN, NM_FAKULTAS dimana NM_MK mengandung kata ‘is’*/
SELECT t3."NM_MK", t2."NM_JURUSAN", t1."NM_FAKULTAS"
FROM "Fakultas" t1
INNER JOIN "Jurusan" t2 ON t1."KD_FAKULTAS"=t2."KD_FAKULTAS"
LEFT JOIN "Matakuliah" t3 ON t2."KD_JURUSAN"=t3."KD_JURUSAN"
WHERE t3."NM_MK" like '%is%'
GROUP BY "NM_MK","NM_FAKULTAS","NM_JURUSAN";

CREATE VIEW "SOAL12" AS SELECT t3."NM_MK", t2."NM_JURUSAN", t1."NM_FAKULTAS"
FROM "Fakultas" t1
INNER JOIN "Jurusan" t2 ON t1."KD_FAKULTAS"=t2."KD_FAKULTAS"
LEFT JOIN "Matakuliah" t3 ON t2."KD_JURUSAN"=t3."KD_JURUSAN"
WHERE t3."NM_MK" like '%is%'
GROUP BY "NM_MK","NM_FAKULTAS","NM_JURUSAN";

/*13	Tampilkan data NM_KOTA, JML_DOSEN*/
SELECT "ALAMAT" AS "NM_KOTA", COUNT("KD_DOSEN") AS "JML_DOSEN"
FROM "Dosen"
GROUP BY "ALAMAT";

CREATE VIEW "SOAL13" AS SELECT "ALAMAT" AS "NM_KOTA", COUNT("KD_DOSEN") AS "JML_DOSEN"
FROM "Dosen"
GROUP BY "ALAMAT";

/*14	Tampilkan data NM_DOSEN, NM_MK, NM_JURUSAN, NM_FAKULTAS, NM_KELAS, NM_RUANG*/
SELECT tDosen."NM_DOSEN", tMatkul."NM_MK", tJurusan."NM_JURUSAN", tFakultas."NM_FAKULTAS", tKelas."NM_KELAS", tRuangan."NM_RUANG"
FROM "Fakultas" tFakultas
JOIN "Jurusan" tJurusan ON tFakultas."KD_FAKULTAS"=tJurusan."KD_FAKULTAS"
JOIN "Matakuliah" tMatkul ON tJurusan."KD_JURUSAN"=tMatkul."KD_JURUSAN"
JOIN "Kelas" tKelas ON tMatkul."KD_MK"=tKelas."KD_MK"
JOIN "TabelRuang" tRuangan ON tKelas."KD_RUANG"=tRuangan."KD_RUANG"
JOIN "Dosen" tDosen ON tDosen."KD_DOSEN"=tKelas."KD_DOSEN"

/*15	Tampilkan data NM_DOSEN, NM_MK, NM_JURUSAN, NM_FAKULTAS*/
SELECT tDosen."NM_DOSEN", tMatkul."NM_MK", tJurusan."NM_JURUSAN", tFakultas."NM_FAKULTAS"
FROM "Fakultas" tFakultas
JOIN "Jurusan" tJurusan ON tFakultas."KD_FAKULTAS"=tJurusan."KD_FAKULTAS"
JOIN "Matakuliah" tMatkul ON tJurusan."KD_JURUSAN"=tMatkul."KD_JURUSAN"
JOIN "Kelas" tKelas ON tMatkul."KD_MK"=tKelas."KD_MK"
JOIN "TabelRuang" tRuangan ON tKelas."KD_RUANG"=tRuangan."KD_RUANG"
JOIN "Dosen" tDosen ON tDosen."KD_DOSEN"=tKelas."KD_DOSEN"

/*15	Tampilkan data NM_DOSEN, JML_MK*/
SELECT n1."NM_DOSEN", COUNT(n2."NM_MK") AS "JML_MK"
FROM "Matakuliah" n2
JOIN "Kelas" n3 ON n2."KD_MK"=n3."KD_MK"
RIGHT JOIN "Dosen" n1 ON n1."KD_DOSEN"=n3."KD_DOSEN"
GROUP BY "NM_DOSEN"

/*16	Berapa rata-rata kapitas Ruangan*/
SELECT AVG("KAPASITAS") FROM "TabelRuang";

/*17	Berapa total mahasiswa yang bisa di tampung dalam semua ruangan*/
SELECT SUM("KAPASITAS") AS "Total Mahasiswa" FROM "TabelRuang";

/*18	Tampilkan Ruangan yang mimiliki kapasitas paling kecil*/
SELECT t1."NM_RUANG", t1."KAPASITAS"
FROM "TabelRuang" t1
JOIN (SELECT MIN("KAPASITAS") AS "M"
FROM "TabelRuang") t2 ON t1."KAPASITAS"=t2."M";

/*19	Tampilkan Ruangan yang mimiliki kapasitas paling besar*/
SELECT t1."NM_RUANG", t1."KAPASITAS"
FROM "TabelRuang" t1
JOIN (SELECT MAX("KAPASITAS") AS "M"
FROM "TabelRuang") t2 ON t1."KAPASITAS"=t2."M";

/*20	Tampilkan data NM_DOSEN, NM_MK, NM_JURUSAN, NM_FAKULTAS, NM_KELAS, NM_RUANG kapasitas ruang paling banyak*/
SELECT tDosen."NM_DOSEN", tMatkul."NM_MK", tJurusan."NM_JURUSAN", tFakultas."NM_FAKULTAS",
tKlas."NM_KELAS",tRuang."NM_RUANG", tRuang."KAPASITAS"
FROM "Fakultas" tFakultas
JOIN "Jurusan" tJurusan ON tFakultas."KD_FAKULTAS"=tJurusan."KD_FAKULTAS"
JOIN "Matakuliah" tMatkul ON tJurusan."KD_JURUSAN"=tMatkul."KD_JURUSAN"
JOIN "Kelas" tKlas ON tMatkul."KD_MK"=tKlas."KD_MK"
JOIN "Dosen" tDosen ON tDosen."KD_DOSEN"=tKlas."KD_DOSEN"
JOIN "TabelRuang" tRuang ON tRuang."KD_RUANG"=tKlas."KD_RUANG"
JOIN (SELECT MAX("KAPASITAS") AS "MX"
FROM "TabelRuang") t2 ON tRuang."KAPASITAS"=t2."MX";

/*21	Tampilkan data NIM, NM_MHS, NM_JURUSAN, NM_FAKULTAS*/
SELECT tMhs."NIM",tMhs."NM_MHS",tJurusan."NM_JURUSAN", tFakultas."NM_FAKULTAS"
FROM "Fakultas" tFakultas
JOIN "Jurusan" tJurusan ON tFakultas."KD_FAKULTAS"=tJurusan."KD_FAKULTAS"
JOIN "Mahasiswa" tMhs ON tJurusan."KD_JURUSAN"=tMhs."KD_JURUSAN";

/*22	Tampilkan data NM_FAKULTAS, NM_JURUSAN, JML_MHS*/
SELECT tJurusan."NM_JURUSAN", tFakultas."NM_FAKULTAS", COUNT(tMhs."NM_MHS") AS "JML_MAHASISWA"
FROM "Fakultas" tFakultas
JOIN "Jurusan" tJurusan ON tFakultas."KD_FAKULTAS"=tJurusan."KD_FAKULTAS"
JOIN "Mahasiswa" tMhs ON tJurusan."KD_JURUSAN"=tMhs."KD_JURUSAN"
GROUP BY tJurusan."NM_JURUSAN", tFakultas."NM_FAKULTAS";

/*23	Tampilkan data NM_FAKULTAS, NM_JURUSAN, JML_MHS yang JML_MHS paling banyak*/
CREATE VIEW "SOAL23" AS SELECT tJurusan."NM_JURUSAN", tFakultas."NM_FAKULTAS",  COUNT(tMhs."NM_MHS") AS "Jumlah"
FROM "Fakultas" tFakultas
JOIN "Jurusan" tJurusan ON tFakultas."KD_FAKULTAS"=tJurusan."KD_FAKULTAS"
JOIN "Mahasiswa" tMhs ON tJurusan."KD_JURUSAN"=tMhs."KD_JURUSAN"
GROUP BY tJurusan."NM_JURUSAN", tFakultas."NM_FAKULTAS";

/*SELECT t23."NM_JURUSAN", t23."NM_FAKULTAS",  t23."Jumlah"*/
FROM "SOAL23" t23
JOIN(SELECT MAX("Jumlah") AS "JML_MHS" FROM "SOAL23") tHasil
ON t23."Jumlah"=tHasil."JML_MHS"


/*24	Tampilkan data NM_FAKULTAS, NM_JURUSAN, JML_MHS yang JML_MHS paling sedikit*/
CREATE VIEW SOAL23 AS tJurusan."NM_JURUSAN", tFakultas."NM_FAKULTAS",  COUNT(tMhs."NM_MHS") AS "Jumlah"
FROM "Fakultas" tFakultas
JOIN "Jurusan" tJurusan ON tFakultas."KD_FAKULTAS"=tJurusan."KD_FAKULTAS"
JOIN "Mahasiswa" tMhs ON tJurusan."KD_JURUSAN"=tMhs."KD_JURUSAN"
GROUP BY tJurusan."NM_JURUSAN", tFakultas."NM_FAKULTAS";

SELECT t23."NM_JURUSAN", t23."NM_FAKULTAS",  t23."Jumlah"
FROM "SOAL23" t23
JOIN(SELECT MIN("Jumlah") AS "JML_MHS" FROM "SOAL23") tHasil
ON t23."Jumlah"=tHasil."JML_MHS";


/*25	Tampilkan data NM_FAKULTAS, NM_JURUSAN yang TIDAK memiliki mahasiswa*/
SELECT tJurusan."NM_JURUSAN", tFakultas."NM_FAKULTAS",  tMhs."NM_MHS"
FROM "Fakultas" tFakultas
JOIN "Jurusan" tJurusan ON tFakultas."KD_FAKULTAS"=tJurusan."KD_FAKULTAS"
LEFT JOIN "Mahasiswa" tMhs ON tJurusan."KD_JURUSAN"=tMhs."KD_JURUSAN"
GROUP BY tJurusan."NM_JURUSAN", tFakultas."NM_FAKULTAS", tMhs."NM_MHS"
HAVING COUNT(tMhs."NM_MHS")=0;

/*26	Tampilkan data NM_KOTA,JML_MHS dari table Mahasiswa*/
SELECT tMa."ALAMAT" AS "NM_KOTA" , COUNT(tMa."NM_MHS") AS "JML_MHS"
FROM "Mahasiswa" tMa
GROUP BY "NM_KOTA";

/*27	Tampilkan data JK, JML_MHS dari table MHS*/
SELECT tMa."JK" , COUNT(tMa."NM_MHS") AS "JML_MHS"
FROM "Mahasiswa" tMa
GROUP BY "JK";

/*28	Tampilkan data JML_MHS yang memiliki nama ‘Desl’*/
SELECT COUNT("NM_MHS") AS "JML_MHS"
FROM "Mahasiswa" 
WHERE "NM_MHS" LIKE 'Desl%';

/*29	Tampilkan data JML_MHS yang memiliki nama ‘Ratna’*/
SELECT COUNT("NM_MHS") AS "JML_MHS"
FROM "Mahasiswa" 
WHERE "NM_MHS" LIKE 'Ratna%';

/*30	Tampilkan data NIM, NM_MHS, NM_JURUSAN, NM_FAKULTAS, NM_MK, SKS, NILAI*/
SELECT tMhs."NIM", tMhs."NM_MHS", tJurus."NM_JURUSAN", 
tFakul."NM_FAKULTAS", tMatkul."NM_MK", tMatkul."SKS", tKdetil."NILAI"
FROM "Fakultas" tFakul
JOIN "Jurusan" tJurus ON tFakul."KD_FAKULTAS"=tJurus."KD_FAKULTAS"
JOIN "Matakuliah" tMatkul ON tJurus."KD_JURUSAN"=tMatkul."KD_JURUSAN"
JOIN "Kelas" tKelas ON tMatkul."KD_MK"=tKelas."KD_MK"
JOIN "Kelasdetail" tKdetil ON tKelas."KD_KELAS"=tKdetil."KD_KELAS"
JOIN "Mahasiswa" tMhs ON tMhs."NIM"=tKdetil."NIM";

/*31	Tampilkan data NIM, NM_MHS, NM_JURUSAN, NM_FAKULTAS, NM_MK, SKS, BOBOT_NILAI*/
SELECT tMhs."NIM", tMhs."NM_MHS", tJurus."NM_JURUSAN", 
tFakul."NM_FAKULTAS", tMatkul."NM_MK", tMatkul."SKS", tKdetil."NILAI", tBnilai."BOBOT" AS "BOBOT_NILAI"
FROM "Fakultas" tFakul
JOIN "Jurusan" tJurus ON tFakul."KD_FAKULTAS"=tJurus."KD_FAKULTAS"
JOIN "Matakuliah" tMatkul ON tJurus."KD_JURUSAN"=tMatkul."KD_JURUSAN"
JOIN "Kelas" tKelas ON tMatkul."KD_MK"=tKelas."KD_MK"
JOIN "Kelasdetail" tKdetil ON tKelas."KD_KELAS"=tKdetil."KD_KELAS"
JOIN "Mahasiswa" tMhs ON tMhs."NIM"=tKdetil."NIM"
JOIN "BobotNilai" tBnilai ON tBnilai."KD_NILAI"=tKdetil."NILAI";

/*32	Tampilkan data NIM, NM_MHS, NM_JURUSAN, NM_FAKULTAS, NM_MK, (SKS * BOBOT_NILAI) as BOBOT_NILAI*/
SELECT tMhs."NIM", tMhs."NM_MHS", tJurus."NM_JURUSAN", 
tFakul."NM_FAKULTAS", tMatkul."NM_MK", (tMatkul."SKS"*tBnilai."BOBOT") AS "BOBOT_NILAI"
FROM "Fakultas" tFakul
JOIN "Jurusan" tJurus ON tFakul."KD_FAKULTAS"=tJurus."KD_FAKULTAS"
JOIN "Matakuliah" tMatkul ON tJurus."KD_JURUSAN"=tMatkul."KD_JURUSAN"
JOIN "Kelas" tKelas ON tMatkul."KD_MK"=tKelas."KD_MK"
JOIN "Kelasdetail" tKdetil ON tKelas."KD_KELAS"=tKdetil."KD_KELAS"
JOIN "Mahasiswa" tMhs ON tMhs."NIM"=tKdetil."NIM"
JOIN "BobotNilai" tBnilai ON tBnilai."KD_NILAI"=tKdetil."NILAI";

/*33	Tampilkan data NIM, NM_MHS, NM_JURUSAN, NM_FAKULTAS, IPK*/
SELECT tMhs."NIM", tMhs."NM_MHS", tJurus."NM_JURUSAN", 
tFakul."NM_FAKULTAS", tMatkul."NM_MK", (tMatkul."SKS"*tBnilai."BOBOT")/tMatkul."SKS" AS "IPK"
FROM "Fakultas" tFakul
JOIN "Jurusan" tJurus ON tFakul."KD_FAKULTAS"=tJurus."KD_FAKULTAS"
JOIN "Matakuliah" tMatkul ON tJurus."KD_JURUSAN"=tMatkul."KD_JURUSAN"
JOIN "Kelas" tKelas ON tMatkul."KD_MK"=tKelas."KD_MK"
JOIN "Kelasdetail" tKdetil ON tKelas."KD_KELAS"=tKdetil."KD_KELAS"
JOIN "Mahasiswa" tMhs ON tMhs."NIM"=tKdetil."NIM"
JOIN "BobotNilai" tBnilai ON tBnilai."KD_NILAI"=tKdetil."NILAI";



/*34	Tampilkan data NIM, NM_MHS, NM_JURUSAN, NM_FAKULTAS, NM_MK, SKS, NILAI yang statusnya LULUS*/
SELECT tMhs."NIM", tMhs."NM_MHS", tJurus."NM_JURUSAN", 
tFakul."NM_FAKULTAS", tMatkul."NM_MK", tMatkul."SKS", tKdetil."NILAI"
FROM "Fakultas" tFakul
JOIN "Jurusan" tJurus ON tFakul."KD_FAKULTAS"=tJurus."KD_FAKULTAS"
JOIN "Matakuliah" tMatkul ON tJurus."KD_JURUSAN"=tMatkul."KD_JURUSAN"
JOIN "Kelas" tKelas ON tMatkul."KD_MK"=tKelas."KD_MK"
JOIN "Kelasdetail" tKdetil ON tKelas."KD_KELAS"=tKdetil."KD_KELAS"
JOIN "Mahasiswa" tMhs ON tMhs."NIM"=tKdetil."NIM"
JOIN "BobotNilai" tBnilai ON tBnilai."KD_NILAI"=tKdetil."NILAI"
WHERE tKdetil."STATUS"='LULUS';



/*35	Tampilkan data NIM, NM_MHS, NM_JURUSAN, NM_FAKULTAS, NM_MK, SKS, NILAI yang statusnya MENGULANG*/
SELECT tMhs."NIM", tMhs."NM_MHS", tJurus."NM_JURUSAN", 
tFakul."NM_FAKULTAS", tMatkul."NM_MK", tMatkul."SKS", tKdetil."NILAI"
FROM "Fakultas" tFakul
JOIN "Jurusan" tJurus ON tFakul."KD_FAKULTAS"=tJurus."KD_FAKULTAS"
JOIN "Matakuliah" tMatkul ON tJurus."KD_JURUSAN"=tMatkul."KD_JURUSAN"
JOIN "Kelas" tKelas ON tMatkul."KD_MK"=tKelas."KD_MK"
JOIN "Kelasdetail" tKdetil ON tKelas."KD_KELAS"=tKdetil."KD_KELAS"
JOIN "Mahasiswa" tMhs ON tMhs."NIM"=tKdetil."NIM"
JOIN "BobotNilai" tBnilai ON tBnilai."KD_NILAI"=tKdetil."NILAI"
WHERE tKdetil."STATUS"='MENGULANG';


