NOMOR 1:

SELECT t1."nama_jurusan", COUNT(t2."nama_mahasiswa") as "JML_MHS"
FROM jurusan t1
JOIN mahasiswa t2 ON t1."kode_jurusan"=t2."kode_jurusan"
GROUP BY "nama_jurusan";

======================================================================

NOMOR 2:

select tmhs."kode_mahasiswa", tmhs."nama_mahasiswa", 
tjurus."nama_jurusan", tagama."deskripsi" as "agama"
from jurusan tjurus
join mahasiswa tmhs on tjurus."kode_jurusan"=tmhs."kode_jurusan"
join agama tagama on tagama."kode_agama"=tmhs."kode_agama"
where "kode_mahasiswa"='M001';
========================================================================

NOMOR 3:



=======================================================================

NOMOR 4:
SELECT * from mahasiswa tmhs
join nilai tnilai on tmhs."kode_mahasiswa"=tnilai."kode_mahasiswa"
join ujian tuji on tuji."kode_ujian"= tnilai."kode_ujian"
where "nilai">80 and "status_ujian"='Aktif';

=====================================================================

NOMOR 5:

Select tmhs."kode_mahasiswa", tmhs."nama_mahasiswa", count(tnilai."kode_mahasiswa") as "BerapaKaliUjian"
from mahasiswa tmhs
join nilai tnilai on tmhs."kode_mahasiswa"=tnilai."kode_mahasiswa"
group by tmhs."kode_mahasiswa", tmhs."nama_mahasiswa"
having count(tnilai."kode_mahasiswa")>1;

=====================================================================

NOMOR 6;
select tmhs."kode_mahasiswa", tmhs."nama_mahasiswa", tjurus."nama_jurusan", tagama."deskripsi" as "NamaAgama", 
tdosen."nama_dosen", tjurus."status_jurusan" as "Status_Jurusan", tTipeDosen."deskripsi" as "Desk_Dosen"
from jurusan tjurus
join mahasiswa tmhs on tjurus."kode_jurusan"=tmhs."kode_jurusan"
join dosen tdosen on tjurus."kode_jurusan"=tdosen."kode_jurusan"
join agama tagama on tagama."kode_agama"=tmhs."kode_agama"
join type_dosen tTipeDosen on tTipeDosen."kode_type_dosen"=tdosen."kode_type_dosen"
where "kode_mahasiswa"='M001';

=====================================================================

NOMOR 7:
membuat view:
CREATE VIEW "PERKOTAAN" AS SELECT "kode_kota", "nama_kota" FROM kota

Nampilkan view:
SELECT * FROM public."PERKOTAAN"

====================================================================

NOMOR 8:

create table kota(
	kode_kota char (5),
	nama_kota varchar (50)
);

INSERT INTO kota VALUES('K001','Bandung');
INSERT INTO kota VALUES('K002','Ciamis');
INSERT INTO kota VALUES('K003','Jakarta Pusat');
INSERT INTO kota VALUES('K004','Surabaya');

====================================================================
NOMOR 9:

Membuat field baru di posgre:
1. klik kanan tabel mahasiswa.
2. klik properties.
3. klik tab columns.
4. klik Add new Rows.
5. Input data >>> name='kode_kota', tipe data=character, length=5
6. klik save.

Input data untuk kode_kota:
1. klik kanan tabel mahasiswa
2. truncate -> klik truncate 
3. klik ok

insert datanya ulang lewat query tool
INSERT INTO mahasiswa VALUES ('M001', 'Budi Gunawan', 'Jl. Mawar No 3 RT 05 Cicalengka, Bandung', 'A001', 'J001', 'K001');
INSERT INTO mahasiswa VALUES ('M002', 'Rinto Rahardjo', 'Jl. Kebagusan No. 33 RT04 RW06 Bandung', 'A002', 'J002', 'K001');
INSERT INTO mahasiswa VALUES ('M003', 'Asep Saepudin', 'Jl. Sumatera No.12 RT02 RW01, Ciamis', 'A001', 'J003', 'K002');
INSERT INTO mahasiswa VALUES ('M004', 'M. Hafif Isbullah', 'Jl. Jawa No 01 RT01 RW01, Jakarta Pusat', 'A001', 'J001', 'K003');
INSERT INTO mahasiswa VALUES ('M005', 'Cahyono', 'JL. Niagara No. 54 RT01 RW09, Surabaya', 'A003', 'J002', 'K004');

=====================================================================

NOMOR 10;

