SELECT tMhs."NIM", tMhs."NM_MHS", tJurus."NM_JURUSAN", 
tFakul."NM_FAKULTAS", tMatkul."NM_MK", tMatkul."SKS", tKdetil."NILAI"
FROM "Fakultas" tFakul
JOIN "Jurusan" tJurus ON tFakul."KD_FAKULTAS"=tJurus."KD_FAKULTAS"
JOIN "Matakuliah" tMatkul ON tJurus."KD_JURUSAN"=tMatkul."KD_JURUSAN"
JOIN "Kelas" tKelas ON tMatkul."KD_MK"=tKelas."KD_MK"
JOIN "Kelasdetail" tKdetil ON tKelas."KD_KELAS"=tKdetil."KD_KELAS"
JOIN "Mahasiswa" tMhs ON tMhs."NIM"=tKdetil."NIM"
JOIN "BobotNilai" tBnilai ON tBnilai."KD_NILAI"=tKdetil."NILAI"
WHERE tKdetil."STATUS"='LULUS';

