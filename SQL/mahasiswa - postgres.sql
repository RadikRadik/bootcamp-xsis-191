DROP TABLE IF EXISTS agama;
CREATE TABLE agama  (
  KODE_AGAMA char(5),
  DESKRIPSI varchar(20)
);

INSERT INTO agama VALUES ('A001', 'Islam');
INSERT INTO agama VALUES ('A002', 'Kristen');
INSERT INTO agama VALUES ('A003', 'Katolik');
INSERT INTO agama VALUES ('A004', 'Hindu');
INSERT INTO agama VALUES ('A005', 'Budha');

DROP TABLE IF EXISTS dosen;
CREATE TABLE dosen  (
  KODE_DOSEN char(5),
  NAMA_DOSEN varchar(100),
  KODE_JURUSAN char(5),
  KODE_TYPE_DOSEN char(5)
);

INSERT INTO dosen VALUES ('D001', 'Prof. Dr. Retno Wahyuningsih', 'J001', 'T002');
INSERT INTO dosen VALUES ('D002', 'Prof. Roy M. Soetikno', 'J002', 'T001');
INSERT INTO dosen VALUES ('D003', 'Prof. Hendri A. Verbrugh', 'J003', 'T002');
INSERT INTO dosen VALUES ('D004', 'Prof. Risma Suparwata', 'J004', 'T002');
INSERT INTO dosen VALUES ('D005', 'Prof. Amir Sjarifuddin Madjid, MM, MBA', 'J005', 'T001');

DROP TABLE IF EXISTS jurusan;
CREATE TABLE jurusan  (
  KODE_JURUSAN char(5),
  NAMA_JURUSAN varchar(50),
  STATUS_JURUSAN varchar(100)
);

INSERT INTO jurusan VALUES ('J001', 'Teknik Informatika', 'Aktif');
INSERT INTO jurusan VALUES ('J002', 'Manajemen Informatika', 'Aktif');
INSERT INTO jurusan VALUES ('J003', 'Sistem Informasi', 'Non Aktif');
INSERT INTO jurusan VALUES ('J004', 'Sistem Komputer', 'Aktif');
INSERT INTO jurusan VALUES ('J005', 'Komputer Science', 'Non Aktif');

DROP TABLE IF EXISTS mahasiswa;
CREATE TABLE mahasiswa  (
  KODE_MAHASISWA char(5),
  NAMA_MAHASISWA varchar(20),
  ALAMAT varchar(200),
  KODE_AGAMA char(5),
  KODE_JURUSAN char(5)
);

INSERT INTO mahasiswa VALUES ('M001', 'Budi Gunawan', 'Jl. Mawar No 3 RT 05 Cicalengka, Bandung', 'A001', 'J001');
INSERT INTO mahasiswa VALUES ('M002', 'Rinto Rahardjo', 'Jl. Kebagusan No. 33 RT04 RW06 Bandung', 'A002', 'J002');
INSERT INTO mahasiswa VALUES ('M003', 'Asep Saepudin', 'Jl. Sumatera No.12 RT02 RW01, Ciamis', 'A001', 'J003');
INSERT INTO mahasiswa VALUES ('M004', 'M. Hafif Isbullah', 'Jl. Jawa No 01 RT01 RW01, Jakarta Pusat', 'A001', 'J001');
INSERT INTO mahasiswa VALUES ('M005', 'Cahyono', 'JL. Niagara No. 54 RT01 RW09, Surabaya', 'A003', 'J002');

DROP TABLE IF EXISTS nilai;
CREATE TABLE nilai  (
  KODE_NILAI char(5),
  KODE_MAHASISWA char(5),
  KODE_UJIAN char(5),
  NILAI decimal(10, 0)
);

INSERT INTO nilai VALUES ('N001', 'M004', 'U001', 90);
INSERT INTO nilai VALUES ('N002', 'M001', 'U001', 80);
INSERT INTO nilai VALUES ('N003', 'M002', 'U003', 85);
INSERT INTO nilai VALUES ('N004', 'M004', 'U002', 95);
INSERT INTO nilai VALUES ('N005', 'M005', 'U005', 70);

DROP TABLE IF EXISTS type_dosen;
CREATE TABLE type_dosen  (
  KODE_TYPE_DOSEN char(5),
  DESKRIPSI varchar(20)
);

INSERT INTO type_dosen VALUES ('T001', 'Tetap');
INSERT INTO type_dosen VALUES ('T002', 'Honoroer');
INSERT INTO type_dosen VALUES ('T003', 'Expertise');

DROP TABLE IF EXISTS ujian;
CREATE TABLE ujian  (
  KODE_UJIAN char(5),
  NAMA_UJIAN varchar(50),
  STATUS_UJIAN varchar(100)
);

INSERT INTO ujian VALUES ('U001', 'Algoritma', 'Aktif');
INSERT INTO ujian VALUES ('U002', 'Aljabar', 'Aktif');
INSERT INTO ujian VALUES ('U003', 'Statistika', 'Non Aktif');
INSERT INTO ujian VALUES ('U004', 'Etika Profesi', 'Non Aktif');
INSERT INTO ujian VALUES ('U005', 'Bahasa Inggris', 'Aktif');