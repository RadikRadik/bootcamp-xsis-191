package hackerRankk.Implementation;

public class ArraySum1 {
	static int sumArray(int[]arr) {
		int hasil=0;
		for(int i=0; i<arr.length;i++) {
			hasil+=arr[i];
		}
		return hasil;
	}
	public static void main(String[] args) {
		int[] arr= {2,4,44,2,6,7,4,3};
		int hasil=sumArray(arr);
		System.out.println("Output: "+hasil);
	}
}
