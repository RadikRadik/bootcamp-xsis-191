package hackerRankk.Implementation;

import java.io.IOException;
import java.util.Scanner;

public class DiagonalDiffernce {
	static int diagDiff(int[][] arr) {
		int a=0; int b=0;
		for(int i=0; i<arr.length;i++) {
			a=a+arr[i][i];
			b=b+arr[i][arr.length-1-i];
		}
		
		if(a>b) {
			return a-b;
		}else
		{
			return b-a;
		}
	}
	//////////////////////////////
	//repeat1
	/////////////////////////////
	static int diagDiff1(int[][]arr) {
		int a=0,b=0;
		for(int i=0; i<arr.length;i++) {
			a+=arr[i][i];
			b+=arr[i][arr.length-1-i];
		}if(a>b){
			return a-b;
		}else {
			return b-a;
		}
		
	}
	
	
	/////////////////////////
	//repeat2
	/////////////////////////
	static int diagDiff2(int[][]arr) {
		int a=0,b=0;
		for (int i=0;i<arr.length;i++){
			a+=arr[i][i];
			a+=arr[i][arr.length-1-i];
		}
		if(a>b) {
			return a-b;
		}else {
			return b-a;
		}
	}
	
	
	///////////////////////
	//REPEAT 3
	/////////////////////
	static int diagDiff3(int[][]arr) {
		int a=0,b=0;
		for(int i=0;i<arr.length;i++) {
			a+=arr[i][i];
			b+=arr[i][arr.length-1-i];
		}
		if(a>b) {
			return a-b;
		}else {
			return b-a;
		}
	}
	
	////////////////////////
	//Repeat4///////////
	/////////////////////
	static int diagDiff4(int[][] arr) {
		int a=0,b=0;
		for(int i=0; i<arr.length;i++) {
			a+=arr[i][i];
			b+=arr[i][arr.length-1-i];
		}
		if(a>b) {
			return a-b;
			
		}else {
			return b-a;
		}
	}
	
	/////////////////
	//repeat5
	////////////////
	static int diagDiff5(int[][]arr) {
		int a=0,b=0;
		for(int i=0;i<arr.length;i++) {
			a+=arr[i][i];
			b+=arr[i][arr.length -1-i];
		}
		if(a>b) {
			return a-b;
		}else {
			return b-a;
		}
	}
	
	public static void main(String[] args) throws IOException {
		/*Scanner scn=new Scanner (System.in);
		System.out.println("Input n row :");
		int nRow= scn.nextInt();
		
		int[][] arr = new int[nRow][nRow];
		
		//int angka=scn.nextInt();
		for(int i=0; i<arr.length;i++) {
			int angka=1;
			for(int j=0; j<arr[i].length;j++) {
				arr[i][j]=angka;
				angka+=3;
			}
		}
		
		for (int k=0;k<arr.length;k++) {
			//print dari kiri ke kanan
			for(int m=0; m<arr[k].length;m++) {
				System.out.print(arr[k][m]+"\t");
			}//pindah baris
			System.out.println("\n");
		}
		
		int hasil=diagDiff(arr);
		System.out.println(hasil);*/
		int[][] arr = {{44,54,30},{78,54,9}};
		System.out.println(diagDiff(arr));
			
	}
}
