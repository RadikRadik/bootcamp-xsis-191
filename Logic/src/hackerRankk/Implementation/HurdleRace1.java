package hackerRankk.Implementation;

public class HurdleRace1 {
	public static int hurdleRace(int k, int[] height) {
		int max = height[0];
		for (int i=0; i<height.length;i++) {
			if(height[i]>max)
				max=height[i];
		}
		if(k<max)
			return max-k;
		else
			return 0;
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println(hurdleRace(4, new int[] {1, 6, 3, 5, 2	}));
	}

}
