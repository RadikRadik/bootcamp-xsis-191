package hackerRankk.Implementation;
//jika ada huruf yang kembar sesudah huruf tersebut maka itunglah berapa yg mesti dihapus;
//contoh AAAAAA -> A(AAAAA) sebanyak 5 huruf A yg mesti dihapus.
public class AlternatingChar {
	
	static int alternatingChar(String s) {
		int count=0;
		int j=1;
		char[]ar=s.toCharArray();
		
		for(int i=0;i<ar.length-1;i++) {
			//jika ar di i dan j sama maka di count
			if(ar[i]==ar[j]&&j<ar.length) {
				count++;
			}j++;//lanjut ke array berikutnya
		}return count;
	}
	
	
	static int alternatingChar1(String s) {
		int count=0;
		int j=1;
		char[]ar=s.toCharArray();
		
		for(int i=0; i<ar.length; i++) {
			if(ar[i]==ar[j]&&j<ar.length) {
				count++;
			}j++;
		}return count;
	}
	
	static int alternatingChar2(String s) {
		int count=0;
		int j=1;
		char[]ar=s.toCharArray();
		
		for(int i=0; i<ar.length; i++) {
			if(ar[i]==ar[j]&&j<ar.length) {
				count++;
			}j++;
		}return count;	
	}
	public static void main(String[] args) {
		System.out.println(alternatingChar("AAAA"));
		System.out.println(alternatingChar("ABAABAA"));
		System.out.println(alternatingChar("ABABABA"));
		System.out.println(alternatingChar("AABBABBA"));
		System.out.println(alternatingChar("BBBAABBBAA"));
		
	}
}
