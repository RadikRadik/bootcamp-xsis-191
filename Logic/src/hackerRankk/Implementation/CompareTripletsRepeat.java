package hackerRankk.Implementation;

import java.util.ArrayList;
import java.util.List;

//repeat 5kali digabung disini
public class CompareTripletsRepeat {
	//repeat1
	static List<Integer> compareTriplets(List<Integer> a, List<Integer> b) {
        List<Integer> result = new ArrayList<Integer>();
        
        result.add(0);result.add(0);
        
        int nAlice=0;
        int nBob=0;
        
        for(int i=0;i<a.size();i++) {
        	if(a.get(i)>b.get(i)) {
        		nAlice++;
        		result.set(0, nAlice);
        	}
        	if(a.get(i)<b.get(i)) {
        		nBob++;
        		result.set(1, nBob);
        	}
        }return result;
	}

	public static void main(String[] args) {
		//repeat1
		List<Integer>a=new ArrayList<Integer>();
		a.add(11);
		a.add(25);
		a.add(14);
		
		List<Integer>b=new ArrayList<Integer>();
		b.add(11);
		b.add(25);
		b.add(14);
		
		/* ======================	
		 * Repeat2
		 * =======================
		 * List<Integer>a=new ArrayList<Integer>();
		 * a.add(1);a.add(2);a.add(3);
		 * 
		 * List<Integer>a=new ArrayList<Integer>();
		 * b.add(3);b.add(3);b.add(3);
		 * 
		 * ===================	
		 * Repeat3
		 * =================
		 * List<Integer>a=new ArrayList<Integer>();
		 * a.add(11);a.add(22);a.add(33);
		 * 
		 * List<Integer>a=new ArrayList<Integer>();
		 * b.add(21);b.add(31);b.add(41);
		 * 
		 * ==========================	
		 * Repeat4
		 * =======================
		 * List<Integer>a=new ArrayList<Integer>();
		 * a.add(1);a.add(2);a.add(3);
		 * 
		 * List<Integer>a=new ArrayList<Integer>();
		 * b.add(4);b.add(3);b.add(5);
		 * 
		 * ======================	
		 * Repeat5
		 * =======================
		 * List<Integer>a=new ArrayList<Integer>();
		 * a.add(1);a.add(2);a.add(3);
		 * 
		 * List<Integer>a=new ArrayList<Integer>();
		 * b.add(1);b.add(2);b.add(3);
		 * */
		
		for (Integer item: compareTriplets(a, b)) {
			System.out.println(item+"\t");
		}
	}
	
	//repeat2
	static List<Integer> CompareTrips2(List<Integer> a,List<Integer> b){
		List<Integer> result = new ArrayList<Integer>();
		result.add(0);
		result.add(0);
		
		int nAlice=0; int nBob=0;
		
		for(int i=0; i<a.size();i++) {
			if(a.get(i)>b.get(i)) {
				nAlice++;
				result.set(0, nAlice);
			}
			if(a.get(i)<b.get(i)) {
				nBob++;
				result.set(1,nBob);
			}
		}
		return result;
	}
	
	//repeat3
	static List<Integer> CompareTrips3(List<Integer>a, List<Integer> b){
		List<Integer> result=new ArrayList<Integer>();
		result.add(0); result.add(0);
		
		int nAlice=0, nBob=0;
		
		for(int i=0; i<a.size();i++) {
			if(a.get(i)>b.get(i)) {
				nAlice++;
				result.set(0, nAlice);
			}
			if(a.get(i)<b.get(i)) {
				nBob++;
				result.set(1, nBob);
			}
		}return result;
	}
	
	//repeat4
	static List<Integer> compareTrips4(List<Integer>a, List<Integer>b)
	{
		List<Integer> hasil=new ArrayList<Integer>();
		hasil.add(0);hasil.add(0);
		
		int nAlice=0, nBob=0;
		for(int i=0;i<a.size();i++) {
			if(a.get(i)>b.get(i)) {
				nAlice++;
				hasil.set(0, nAlice);
			}
			if(a.get(i)<b.get(i)) {
				nBob++;
				hasil.set(1, nBob);
			}
		}return hasil;
	}
	
	//repeat5
	static List<Integer> compartirp5(List<Integer>a, List<Integer>b)
	{
		List<Integer> hasil=new ArrayList<Integer>();
		hasil.add(0); hasil.add(0);
		int nAlice=0, nBob=0;
		for(int i=0; i<a.size();i++) {
			if(a.get(i)>b.get(i)) {
				nAlice++;
				hasil.set(0, nAlice);
			}
			if(a.get(i)>b.get(i)) {
				nBob++;
				hasil.set(1, nBob);
			}
		}return hasil;
	}
}
