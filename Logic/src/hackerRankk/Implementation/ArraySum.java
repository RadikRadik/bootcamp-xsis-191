package hackerRankk.Implementation;

//import java.io.BufferedWriter;
//import java.io.FileWriter;
import java.io.IOException;
//import java.util.Scanner;

public class ArraySum {
	
	//ini method yg return value
	   static int simpleArraySum(int[] ar) {
	        int result=0;
	        for(int i=0; i<ar.length;i++){
	            result=result+ar[i];
	        }
	        return result;

	    }

	    //private static final Scanner scanner = new Scanner(System.in);
	   //void method non return value
	    public static void main(String[] args) throws IOException {
	        int[] ar= {2,4,6,2,5,6,5};
	        
	        int hasil = simpleArraySum(ar);
	        
	        System.out.println(hasil);
	    }
}
