package hackerRankk.Implementation;

public class HurdleRace6 {

	public static int hurdleRace(int k, int[] tinggi) {
		int max=tinggi[0];
		for(int i=0; i<tinggi.length;i++) {
			if(tinggi[i]>max) max=tinggi[i];
		}
		
		if(k<max) return max-k;
		else return 0;
	}
	public static void main(String[] args) {
		System.out.println(hurdleRace(4, new int[] {1, 6, 3, 5, 2	}));
	}
}
