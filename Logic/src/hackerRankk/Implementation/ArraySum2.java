package hackerRankk.Implementation;

public class ArraySum2 {
	static int sumArr(int[]arr) {
		int hasil=0;
		for(int i=0; i<arr.length;i++) {
			hasil+=arr[i];
		}
		return hasil;
	}
	public static void main(String[] args) {
		int[]arr= {3,4,2,2,4,6,8,5,2,8,3,8,22,4,7};
		int hasil=sumArr(arr);
		System.out.println("Hasil: "+hasil);
	}
}
