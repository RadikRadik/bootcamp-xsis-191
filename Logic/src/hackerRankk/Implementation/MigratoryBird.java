package hackerRankk.Implementation;
//cari nilai array paling banyak, jika ada yang kembar ambil yang kecil idnya.
//import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

//import com.sun.glass.ui.Size;

public class MigratoryBird {
	
	
	public static void main(String[] args) {
		
	}
	
	static int migratoryBird(List<Integer> arr) {
		Map<Integer, Integer> map=new HashMap<>();
		
		//untuk mendapatkan arraynya
		for(int i=0; i<arr.size();i++) {
			//arr.get(i);
			if(map.containsKey(arr.get(i))) {
				int nHasil = map.get(arr.get(i));
				nHasil++;
				map.put(arr.get(i), nHasil); //mendapatkan nilai arr
			}else {
				map.put(arr.get(i), 1);
			}
				
		}
		int max=0, key =0;
		for (Map.Entry<Integer, Integer> item : map.entrySet()) {
			if(item.getValue()>max) {
				max=item.getValue();
				key=item.getKey();
				
			}
			if (item.getValue()==max && item.getKey()<key) {
				max=item.getKey();
			}
			
		}return 0;
		
		
	}
	
	static int migBird(List<Integer>arr) {
		Map<Integer, Integer> map= new HashMap<>();
		
		for (int i=0; i<arr.size();i++) {
		if(map.containsKey(arr.get(i))) {
			int nHasil = map.get(arr.get(i));
			nHasil++;
			map.put(arr.get(i), nHasil);
		}else {
			map.put(arr.get(i), 1);
		}}
		
		
		int maximum=0, key=0;
		for(Map.Entry<Integer, Integer> item : map.entrySet()) {
			if(item.getValue()>maximum) {
				maximum=item.getValue();
				key= item.getKey();
			}
			if (item.getValue()==maximum && item.getKey()<key) {
				maximum=item.getKey();
			}
		}return 0;
	}
	
}
