package hackerRankk.Java;

import java.util.HashMap;
//import java.util.HashSet;
import java.util.Map;

public class JavaAnagram {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println(isAnagram("Hello", "loleH"));
	}
	
	public static boolean isAnagram(String a, String b) {
		Map<String, Integer> map = new HashMap<>();
		
		String[]arr1 = a.toLowerCase().split("");
		String[]arr2 = b.toLowerCase().split("");
		for(int i=0; i<arr1.length;i++) {
			//ngecek string kesatu dan ditampung ke map A
			if(map.containsKey(arr1[i])) {
				int nHasil=map.get(arr1[i]);
				nHasil++;
				map.put(arr1[i], nHasil);
			}else {
				map.put(arr1[i], 1);
			}
		}
		for(int i=0; i<arr2.length;i++) {
			if(!map.containsKey(arr2[i])) {
				return false;
			}
			int nHasil= map.get(arr2[i]);
			if(nHasil==0) {
				return false;
			}else {
				nHasil--;
				map.put(arr2[i], nHasil);
			}
		}
		
		return true;
		
		
	}

}
