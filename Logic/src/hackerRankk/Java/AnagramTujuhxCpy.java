package hackerRankk.Java;

import java.util.HashMap;
import java.util.Map;

public class AnagramTujuhxCpy {
	
	public static boolean isAnagram(String a, String b) {
		Map<String, Integer> mapp = new HashMap<>();
		
		String[] z1 = a.toLowerCase().split("");
		String[] z2 = b.toLowerCase().split("");
		
		for (int i=0; i<z1.length;i++) {
			//ngechek string kesatu dan tampung ke map a
			if(mapp.containsKey(z1[i])) {
				int nHasil = mapp.get(z1[i]);
				nHasil++;
				mapp.put(z1[i], nHasil);
			}
		}
		
		for(int i= 0; i<z2.length;i++) {
			if(!mapp.containsKey(z2[i])) {
				return false;
			}
			
			int nHasil=mapp.get(z2[i]);
			if (nHasil==0) {
				return false;
			}
			else {
				nHasil--;
				mapp.put(z2[i], nHasil);
			}
		}return true;
	}
	
	
	
	
	

	
	
	public static boolean isAnagram1(String a, String b) {
		Map<String, Integer> mapp = new HashMap<>();
		
		String[] z1 = a.toLowerCase().split("");
		String[] z2 = b.toLowerCase().split("");
		for (int i=0; i<z1.length;i++) {
			if(mapp.containsKey(z1[i])) {
				int nHasil = mapp.get(z1[i]);
				nHasil++;
				mapp.put(z1[i], nHasil);
			}
		}
		for(int i= 0; i<z2.length;i++) {
			if(!mapp.containsKey(z2[i])) {
				return false;
			}
			int nHasil=mapp.get(z2[i]);
			if (nHasil==0) {
				return false;
			}else {
				nHasil--;
				mapp.put(z2[i], nHasil);
			}
		}return true;
	}
	
	

	
	public static boolean isAnagram2(String a, String b) {
		Map<String, Integer> mapp = new HashMap<>();
		
		String[] a1 = a.toLowerCase().split("");
		String[] b2 = b.toLowerCase().split("");
		for (int i=0; i<a1.length;i++) {
			if(mapp.containsKey(a1[i])) {
				int nHasil = mapp.get(a1[i]);
				nHasil++;
				mapp.put(a1[i], nHasil);
			}
		}
		for(int i= 0; i<b2.length;i++) {
			if(!mapp.containsKey(b2[i])) {
				return false;
			}
			int nHasil=mapp.get(b2[i]);
			if (nHasil==0) {
				return false;
			}else {
				nHasil--;
				mapp.put(b2[i], nHasil);
			}
		}return true;
	}
	
	
	
	public static void main(String[] args) {
		System.out.println(isAnagram("Hello", "loleH"));
		System.out.println(isAnagram1("Hello", "elloH"));
		System.out.println(isAnagram2("kabar", "rabak"));
		//System.out.println(isAnagram3("Gantungan", "nagnutgan"));
		//System.out.println(isAnagram4("Hello", "ellloH"));
		//System.out.println(isAnagram5("pakabar", "karabpa"));
	}
	

}

