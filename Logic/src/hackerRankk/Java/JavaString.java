package hackerRankk.Java;

import java.util.Scanner;

public class JavaString {

    public static void main(String[] args) {
        
        Scanner sc=new Scanner(System.in);
        System.out.println("Input String a; ");
        String A=sc.next();
        System.out.println("Input String b: ");
        String B=sc.next();
        
        int nPanjangB=0;
        int nPanjangA=0;
        
        //itung panjang
        nPanjangA= A.length();
        nPanjangB= B.length();

        int n=nPanjangA+nPanjangB;

        System.out.println(n);
        
        //bandingin 2string mana yg paling panjang
        if(A.compareTo(B)>B.compareTo(A)){
            System.out.println("Yes");
        }else {
            System.out.println("No");
        }
        
        //gabungin 2 string yg diinput
       System.out.printf("%s", A.substring(0,1).toUpperCase()+A.substring(1)+" "+B.substring(0,1).toUpperCase()+B.substring(1));
       


    }
}
