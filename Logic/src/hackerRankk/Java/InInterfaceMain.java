package hackerRankk.Java;

import java.util.Scanner;

public class InInterfaceMain {
	public static void main(String[] args) {
		Scanner scan=new Scanner(System.in);
		InMyCalculator nCalc = new InMyCalculator();
		System.out.println("Implemented : ");
		ImplementedInterfaceNames(nCalc);
	}

    static void ImplementedInterfaceNames(Object o){
        Class[] theInterfaces = o.getClass().getInterfaces();
        for (int i = 0; i < theInterfaces.length; i++){
            String interfaceName = theInterfaces[i].getName();
            System.out.println(interfaceName);
        }
    }
}
