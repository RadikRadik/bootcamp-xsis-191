package hackerRankk.Java;

import java.util.Scanner;

public class JavaLoopSatuCopy3 {
	public static void main(String[] args) {
        Scanner sn= new Scanner(System.in);
        System.out.println("Input N: ");
		int N = sn.nextInt();
        for(int i = 1; i<=10;i++){
            System.out.printf("%d x %d = %d\n", N, i, N*i);
        }
	}
}
