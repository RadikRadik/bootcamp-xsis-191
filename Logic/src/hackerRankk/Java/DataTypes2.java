package hackerRankk.Java;

import java.util.Scanner;

public class DataTypes2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner scn= new Scanner(System.in);
        int t =scn.nextInt();
        for(int i=0; i<t; i++){
            try{
                long x=scn.nextLong();
                System.out.println(x+" can be fitted in:");
                if(x>=-128 && x<=127) 
                	System.out.println("* byte");
                if(x>=-32768 && x<=32767) 
                	System.out.println("* short");
                if(x>=Math.pow(2,31)*-1 && x<=Math.pow(2,31)-1) 
                	System.out.println("* int");
                if(x>=Math.pow(2,63)*-1 && x<=Math.pow(2,63)-1) 
                	System.out.println("* long");
                
            }catch(Exception e){
                System.out.println(scn.next()+" can't be fitted anywhere.");
            }

        }
	}

}
