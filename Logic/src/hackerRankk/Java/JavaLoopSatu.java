package hackerRankk.Java;

import java.util.Scanner;

public class JavaLoopSatu {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
        Scanner sn= new Scanner(System.in);
        System.out.println("Input N: ");
		int N = sn.nextInt();
       
		//looping n perkalian dari 1 sampai 10
        for(int i = 1; i<=10;i++){
            System.out.printf("%d x %d = %d\n", N, i, N*i);
        }
	}

}
