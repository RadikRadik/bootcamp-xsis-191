package Hari05;

public class Busway {
	int NomorBus;
	String StaAwal;
	String StaAkhir;
	String PlatBus;
	String NmaSupir;
	String OperatorBus;
	String TipeBus;
	
	
	public Busway(int NomorBus, String PlatBus, String OperatorBus, String StaAwal, String StaAkhir, String NmaSupir, String TipeBus) {
		this.NomorBus	= NomorBus;
		this.PlatBus 	= PlatBus;
		this.OperatorBus= OperatorBus;
		this.StaAwal	= StaAwal;
		this.StaAkhir	= StaAkhir;
		this.TipeBus	= TipeBus;
		this.NmaSupir	= NmaSupir;
	}
	
	public void cetak() {
		System.out.println("Nomor Bus\t= "+this.NomorBus);
		System.out.println("Nomor Polisi\t= "+this.PlatBus);
		System.out.println("Operator\t= "+this.OperatorBus);
		System.out.println("Stasiun Asal\t= "+this.StaAwal);
		System.out.println("Stasiun Tujuan\t= "+this.StaAkhir);
		System.out.println("Tipe Bus\t= "+this.TipeBus);//double decker, bus derek, mini bus, low floor bus.
		System.out.println("Supir\t\t= "+this.NmaSupir);
	}
}