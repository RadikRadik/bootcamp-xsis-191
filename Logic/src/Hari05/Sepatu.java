package Hari05;

public class Sepatu {
	int UkuranSpatu;
	String Merek;
	String Model;
	String Jenis;
	int Harga;
	
	public Sepatu(int UkuranSpatu, String Merek, String Model, String Jenis, int Harga) {
		this.UkuranSpatu=UkuranSpatu;
		this.Merek=Merek;
		this.Model=Model;
		this.Jenis=Jenis;
		this.Harga=Harga;
	}
	
	public void cetak() {
		System.out.println("Ukuran = "+this.UkuranSpatu);
		System.out.println("Merek = "+this.Merek);
		System.out.println("Model = "+this.Model);
		System.out.println("Jenis = "+this.Jenis);
		System.out.println("Harga = $ "+this.Harga);
	}
}
