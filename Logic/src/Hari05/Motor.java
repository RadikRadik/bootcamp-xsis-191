package Hari05;

public class Motor {
	int nomor;
	String Tipe;
	String Merek;
	int CcMotor;
	String TipeBBM;
	int Liter;
	
	
	public Motor(int nomor, String merek, String Tipe, int CcMotor, String TipeBBM, int Liter) {
		this.nomor=nomor;
		this.Merek=merek;
		this.Tipe=Tipe;
		this.CcMotor=CcMotor;
		this.TipeBBM=TipeBBM;
		this.Liter=Liter;
	}
	
	public void cetak() {
		System.out.println("Nomor = "+this.nomor);
		System.out.println("Merek Motor = "+this.Merek);
		System.out.println("Tipe Motor = "+this.Tipe);
		System.out.println("Besar Mesin = "+this.CcMotor+" CC");
		System.out.println("Bahan Bakar = "+this.TipeBBM);
		System.out.println("Besar Tangki = "+this.Liter+" Littre");
	}
	
}
