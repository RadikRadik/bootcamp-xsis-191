package Hari05;

public class MotorMain {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Motor mtr1= new Motor(1, "Yamaha", "Xmax", 250, "Pertamax Turbo", 7);
		System.out.println("Motor pertama = ");
		mtr1.cetak();
		
		Motor mtr2= new Motor(2, "Yamaha", "Nmax", 150, "Pertamax", 7);
		System.out.println("Motor Kedua = ");
		mtr2.cetak();
		
		Motor mtr3= new Motor(3, "Yamaha", "Mt-25", 250, "Pertamax Turbo", 8);
		System.out.println("Motor Ketiga = ");
		mtr3.cetak();
		
	}

}
