package Hari05;

public class JenisHape {
	int nomor;
	String Merek;
	String ModelHp;
	int BesarBatere;
	String Cpu;
	int Ram;
	int Rom;
	
	
	public JenisHape(int nomor, String Merek, String ModelHp, int Ram, int Rom, String Cpu, int BesarBatere) {
		this.nomor=nomor;
		this.Merek=Merek;
		this.ModelHp=ModelHp;
		this.Ram=Ram;
		this.Rom=Rom;
		this.Cpu=Cpu;
		this.BesarBatere=BesarBatere;
	}
	public void cetak() {
		System.out.println("Nomor = "+this.nomor);
		System.out.println("Merek HP = "+this.Merek);
		System.out.println("Tipe HP = "+this.ModelHp);
		System.out.println("Ram = "+this.Ram+" GB");
		System.out.println("Rom = "+this.Rom+" GB");
		System.out.println("CPU = "+this.Cpu);
		System.out.println("Besar Baterai = "+this.BesarBatere+" m.A.h");}
}
