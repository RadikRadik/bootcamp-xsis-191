package Hari05;

public class Hewan {
	int nomor;
	String JenisHewan;
	String Asal;
	String NamaHewan;
	String Babitat;
	String Makan;
	
	
	public Hewan(int nomor, String NamaHewan, String JenisHewan, String Habitat, String Asal, String Makanan) {
		this.nomor=nomor;
		this.NamaHewan=NamaHewan;
		this.JenisHewan=JenisHewan;
		this.Babitat=Habitat;
		this.Asal=Asal;
		this.Makan=Makanan;
	}
	
	public void cetak() {
		System.out.println("Id = "+this.nomor);
		System.out.println("Nama Hewan = "+this.NamaHewan);
		System.out.println("Jenis Hewan = "+this.JenisHewan);
		System.out.println("Habitat = "+this.Babitat);
		System.out.println("Asal Negara = "+ this.Asal);
		System.out.println(this.Makan);
	}
	
}
