package hari08;

public class Faktorial {
	
	public static int faktorial(int n) {
		if(n==0) {
			return 1;
		}
		return n*faktorial(n-1);
	}
	
	public static void main(String[] args) {
		int num=5;
		System.out.println("Faktorial of "+num+" is "+faktorial(num));
	}		
}
