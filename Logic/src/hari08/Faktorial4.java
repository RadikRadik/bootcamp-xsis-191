package hari08;

public class Faktorial4 {
	public static int faktorial(int n) {
		if(n==0) {
			return 1;
		}
		return n*faktorial(n-1);
	}
	
	public static void main(String[] args) {
		int num=4;
		System.out.println(num+"! adlah "+faktorial(num));
	}
}
