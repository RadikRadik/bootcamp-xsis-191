package hari01;

public class HelloWOrld {
	//private static int batch;

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println("Welcome to Javaaaaaaa");
		makananFavorit();
		//objek itu adalah variable;;;
		
		sampleObject();
		//beda scope kalo membuat nya satu persatu
		
		
	}
	
	public static void makananFavorit() {
		System.out.println("1. Tempe Goreng");
		System.out.println("2. Mie Ayam");
		System.out.println("3. Pisang Goreng");
		System.out.println("4. Ikan");
		System.out.println("5. Ayam Ojolali");
	}
	
	public static void sampleObject() {
		
		Orang or01= new Orang();//proses instansiansi untuk pembuatan objek baru
		or01.nama="ARton";
		or01.alamat="Jalan Lapuk";
		or01.jk="Pria";
		or01.tptLahir="Jakarta Selatan";
		or01.umur=22;
		
		Orang or02= new Orang();
		or02.nama="AnTon";
		or02.alamat="Jalan Kupal";
		or02.jk="Pria";
		or02.tptLahir="Jakarta";
		or02.umur=22;

		Orang or03= new Orang();
		or03.nama="Antangin";
		or03.alamat="Jalan fatmawati";
		or03.jk="Pria";
		or03.tptLahir="Jakarta Selatan";
		or03.umur=23;
		
		Orang or04= new Orang();
		or04.nama="Ana";
		or04.alamat="Jalan Tanah";
		or04.jk="Wanita";
		or04.tptLahir="Jakarta";
		or04.umur=24;
			
		Orang or05= new Orang();
		or05.nama="Ann";
		or05.alamat="Jalan utara";
		or05.jk="Wanita";
		or05.tptLahir="Jakarta";
		or05.umur=23;
		
		or01.cetak();
		or02.cetak();
		or03.cetak();
		or04.cetak();
		or05.cetak();
		
	}

}
