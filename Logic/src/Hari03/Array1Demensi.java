package Hari03;

import java.util.Scanner;



public class Array1Demensi {
	static Scanner scn;
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		ulangArray1();
	}
	
	public static void contohArray() {
		//cara 1
				int[] array = new int [10]; //deklarasi array // indeksnya mulai dari 0-9 atau totalnya 10
				/*enginput nilai indeksnya contoh*/
				// array[index]=nilainya;
				array[0]=1;
				array[1]=2;
				array[2]=3;
				array[3]=4;
				array[4]=5;
				array[5]=6;
				array[6]=7;
				array[7]=8;
				array[8]=9;
				array[9]=10;
				//array[10]=2; bikin error karena indexnya melebihi maksimal indeks yg diberikan
				System.out.print("Panjang array = "+array.length+"\n");
				for(int i = 0; i<array.length;i++) {
					System.out.print(array[i]+"\n");
				}
				//cara 2
				// tipe data reference perlu instansiasi contohnya "new" dibawah // membuat objeknya menggunakan new
				//data reference merupakan tipe data yang dapat diatur.
				int[] array1 = new int[] {1,2,3,4,5};
				System.out.println("\n\n panjang array= "+array1.length);
				for(int j=0; j<array1.length;j++) {
					System.out.println(array1[j]);
				}
	}
	
	public static void soalArray() {
		//soal
		// 3 1 9 3 15 5 => n=6
		scn = new Scanner(System.in);
		System.out.print("Masukan Nilai n : ");
		int n = scn.nextInt();
		System.out.println(n);
		int angka=3; int angka2=1;
		
		int[] array = new int [n]; //memasukkan nilai n ke array
		for(int i=0; i<array.length;i++) {
			//array[i]=angka;
			//jika i genap maka akan di tambah 6
			if(i%2==0) {
				array[i]=angka;
				angka=angka+6;
			}else {
			//jika i ganjil maka akan di tambah 2
				array[i]=angka2;
				angka2=angka2+2;
			}
			System.out.print(array[i]+"\t");
		}
		
	}
	
	//soal array: 3 1 9 3 15 5 dengan n=6
	public static void ulangArray1() {
		scn=new Scanner (System.in);
		System.out.println("Input n = ");
		int nilai = scn.nextInt();
		int ni1=3, ni2=1;
		
		int[] array= new int[nilai];
		for(int i=0; i<array.length; i++) {
			if(i%2==0) {
				array[i]=ni1;
				ni1=ni1+6;
			}else {
				array[i]=ni2;
				ni2=ni2+2;
			}System.out.print(array[i]+"\t");
		}
		
	}

}
