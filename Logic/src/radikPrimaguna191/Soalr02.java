package radikPrimaguna191;

//buat kode untuk mengetahui jika kata yang diketik kalo dibalik bakal sama kaya sebelumnya
//contoh: input; katak
//output: YES //karena jika kata katak dibalik masih jadi katak

//input: kami
//output: NO // karena jika kami dibalik jaki imak, tidak sama dengan kata yang awalnya
import java.util.Scanner;

public class Soalr02 {
	
	//public static string palindrome() {
		
	//}
	
	public static void main(String[] args) {
		
		Scanner scan= new Scanner(System.in);
		System.out.print("Enter String  : " );
		String s=scan.nextLine();
		
		String count="";
		
		for(int i=s.length()-1; i>=0; i--) {
			count+= s.charAt(i);
		}
		if(s.equals(count))
		{
			System.out.println("YES");
		}else System.out.println("NO");
	}

}
