//input  : 2,3,4,5,6

//proses1: 3,4,5,6,2
//proses2: 4,5,6,2,3
//proses3: 5,6,2,3,4

//output : 5,6,2,3,4
package radikPrimaguna191;

import java.util.Scanner;

public class Soal03 {
	//Jawaban salah
	public static int miripMiripAscending(int n) {
		int[] arr=new int[n];
		
		Scanner scn=new Scanner(System.in);
		int sementara;
		System.out.println("Input Nilai arr : ");
		for(int  i=0; i<n;i++) {
			arr[i]=scn.nextInt();
		}
		
		for(int i=0; i<n; i++) {
			for(int j=i+1; j<n;j++) {
				if(arr[i]>arr[j]) {
					sementara=arr[i];
					arr[i]=arr[j];
					arr[n-1]=sementara;
				}
			}
		}
		for(int i=0; i<n;i++) {
			System.out.print(arr[i]+" ");
		}
		//System.out.println(arr);
		return n;
		
	}
	//yang bener yg dibawah ini
	public static int realAnswerSoal3(int n) {
		int[] arr=new int[n];
		Scanner scan=new Scanner(System.in);
		int temp=0;
		System.out.println("Masukkan Angka array : ");
		for(int i=0;i<n;i++) {
			arr[i]=scan.nextInt();
		}
		
		//loop ampe 3x
		//buat ngemindahin element array ke kiri
		for(int i=0;i<3;i++) {
			temp=arr[0];//simpan sementara array pertama ke temp
			//pindahin 1 langkah kekiri buat semua array 
			//klo buat kekanan fornya diganti jadi(int q=n-1;q>=0;q--)
			for(int q=1; q<n;q++) {
				arr[q-1]=arr[q]; //klo yg keknana ini jadi arr[q+1]=arr[q];
			}arr[n-1]=temp; //balikin nilai array yg disimpan ke belakang
		}
		
		for(int r=0;r<n;r++) {
			System.out.print(arr[r]+" ");
		}return temp;
	}
	
	public static void main(String[] args) {
		Scanner scn=new Scanner(System.in);
		int n;
		
		System.out.println("Input banyak n ; "); n=scn.nextInt();
		
		//miripMiripAscending(n);
		realAnswerSoal3(n);
		
		
	}
}
