//pergi ke restoran makan-makan, mesen banyak makanan dan ada makanan laut, tpi salah satu orang alergi dan tidak makan
//jadi harus bagi rata bayar, yg ikan dibagi rata tanpa dengan teman yg alergi, sisa makanannya dibagi rata semuanya
//berapa pada baya makannannya antara yang alergi ama sisanya? (ada tambahan pajak pemerintah dan restoran.)
/*3 orang makan bareng, 1 orang alegi ikan
 * menu
 *  
 * ikan kakap	33k
 * nasi goreng	45k
 * ayam mentega 15k
 * rendang		80k
 * 
 * "ingat: menu pertama selalu menu ikan sisanya bukan ikan"
 * 
 * itungannya: ikan/2 = 16.5k/orang
 * 			   nasgor+ayam+rendang/3=46.7k/orang
 * 
 * bill bayar untuk orang alergi ikan = 46.7k/orang
 * bill bayar untuk 2 orang lainnya   = 46.7k+16.5k=63.2k/orang
 * 
 * 
 * */

package radikPrimaguna191;

import java.util.Scanner;

public class Soal04 {
	
	public static void bayarMakan(float[] harga) {
		float[] totalPajak=new float[harga.length]; 
		float[] pajak=new float[harga.length];
		
		//total masing2 harga dengan pajak 10%dan 5% restoran
		for (int i=0; i<harga.length;i++) {
			pajak[i]=harga[i]*15/100;
			totalPajak[i]=harga[i]+pajak[i];
			//System.out.print(pajak[i]+" ");
			//System.out.println(totalPajak[i]);
		}
		//totalin harga semua kecuali array awal.
		float bayar=0;
		for(int i=1; i<harga.length;i++) {
			bayar+=totalPajak[i];
		}
		
		bayar=bayar/4;//bayar perorang yg tanpa menu ikan
		float bayar1=totalPajak[0]/3;//bayar perorang untuk menu ikannya aja
		float bayar2=bayar+bayar1;// total semua untuk orang yg bayar ama ikan
		
		System.out.println("alergi ikan bayar : "+bayar);
		System.out.println("3 yang laennya bayar : "+bayar2);
	}
	
	public static void main(String[] args) {
		Scanner scan = 	new Scanner(System.in);
		int nNilai;
		System.out.println("masukan berapa makanan yg diorder : ");
		nNilai=scan.nextInt();
		
		float[] harga= new float[nNilai];
		System.out.println("masukan harga makanan : 4");
		for(int i=0; i<nNilai;i++) {
			harga[i]=scan.nextFloat();
		}
		bayarMakan(harga);
	}
	
	
}
