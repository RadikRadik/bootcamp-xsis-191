package radikPrima191Retes;

import java.util.Scanner;

public class Soal03 {

	
	public static void main(String[] args) {
		Scanner scan= new Scanner(System.in);
		int n;
		n=scan.nextInt();
		int [] arr=new int[n];
		System.out.println("masukan nilai array: ");
		for (int k=0; k<n;k++) {
			arr[k]=scan.nextInt();
		}
		//int [] arr={1,2,1,3,4,7,1,1,5,6,1,8};
		int temp=0;
		
		for(int i=0; i<arr.length;i++) {
			for(int j=i+1; j<arr.length;j++) {
				if(arr[i]<arr[j]) {
					temp=arr[i];
					arr[i]=arr[j];
					arr[j]=temp;
				}
			}
		}
		
		for(int i=0;i<arr.length;i++) {
			System.out.print(arr[i]+ "  ");
		}
	}
}
