package com.xsis.demo.controller;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.xsis.demo.model.Fakultas;
import com.xsis.demo.repository.FakultasRepo;

@Controller
public class ApiFakultasController {
	@Autowired
	private FakultasRepo repo1;
	
	private Log log1 = LogFactory.getLog(getClass());
	
	@RequestMapping(value = "/api/fakultas/", method=RequestMethod.GET)
	public ResponseEntity<List<Fakultas>> list(){
		ResponseEntity<List<Fakultas>> hasil = null;
		try {
			List<Fakultas> list = repo1.findAll();
			hasil = new ResponseEntity<List<Fakultas>>(list, HttpStatus.OK);
		} catch (Exception e) {
			// TODO: handle exception
			log1.debug(e.getMessage(),e);
			hasil = new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return hasil;
	}
	
	@RequestMapping(value = "/api/fakultas/", method = RequestMethod.POST)
	public ResponseEntity<Fakultas> create(@RequestBody Fakultas itemm){
		ResponseEntity<Fakultas> hasil=null;
		try {
			repo1.save(itemm);
			hasil = new ResponseEntity<Fakultas>(itemm, HttpStatus.CREATED);
		} catch (Exception e) {
			// TODO: handle exception
			log1.debug(e.getMessage(),e);
			hasil = new ResponseEntity<Fakultas>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return hasil;
	}
	
	@RequestMapping(value = "/api/fakultas/{del}", method = RequestMethod.DELETE)
	public ResponseEntity<Fakultas> delete(@PathVariable (name = "del") Integer id){
		ResponseEntity<Fakultas> hasil = null;
		
		try {
			Fakultas item = repo1.findById(id).orElse(null);
			repo1.delete(item);
			hasil = new ResponseEntity<Fakultas>(item, HttpStatus.OK);
		} catch (Exception e) {
			// TODO: handle exceptionl
			log1.debug(e.getMessage(),e);
			hasil = new ResponseEntity<Fakultas>(HttpStatus.INTERNAL_SERVER_ERROR);
		}return hasil;
	}
	
}
