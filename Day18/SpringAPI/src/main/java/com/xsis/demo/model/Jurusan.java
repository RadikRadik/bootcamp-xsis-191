package com.xsis.demo.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.TableGenerator;

@Entity
@Table(name = "jurusan")
public class Jurusan {
	
	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.TABLE, generator="jurusan_seq")
	@TableGenerator(name = "jurusan_seq", table = "tbl_sequence", pkColumnName = "seq_id",
	valueColumnName= "seq_value", initialValue = 0, allocationSize = 1)
	private Integer id;
	
	@Column(name = "kodeJurusan", nullable = false)
	private String kodeJurusan;
	
	@Column(name = "namaJurusan", nullable = false)
	private String namaString;
	
	@Column(name = "kodeFakultas", nullable = false )
	private String kodeFakultas;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getKodeJurusan() {
		return kodeJurusan;
	}

	public void setKodeJurusan(String kodeJurusan) {
		this.kodeJurusan = kodeJurusan;
	}

	public String getNamaString() {
		return namaString;
	}

	public void setNamaString(String namaString) {
		this.namaString = namaString;
	}

	public String getKodeFakultas() {
		return kodeFakultas;
	}

	public void setKodeFakultas(String kodeFakultas) {
		this.kodeFakultas = kodeFakultas;
	}
	
	
}
