package com.xsis.demo.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.TableGenerator;

@Entity
@Table(name= "fakultas")
public class Fakultas {
	
	@Id
	@Column(name="id")
	@GeneratedValue(strategy= GenerationType.TABLE, generator = "fakultas_seq")
	@TableGenerator(name = "fakultas_seq", table = "tbl_sequence", pkColumnName = "seq_id",
	valueColumnName = "seq_value", initialValue = 0, allocationSize=1)
	private Integer id;
	
	@Column(name = "kodeFakultas", nullable=false)
	private String kodeFakultas;
	
	@Column(name = "namaFakultas", nullable=false)
	private String namaFakultas;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getKodeFakultas() {
		return kodeFakultas;
	}

	public void setKodeFakultas(String kodeFakultas) {
		this.kodeFakultas = kodeFakultas;
	}

	public String getNamaFakultas() {
		return namaFakultas;
	}

	public void setNamaFakultas(String namaFakultas) {
		this.namaFakultas = namaFakultas;
	}

	

	
	
	
}
