package com.xsis.demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.xsis.demo.model.Biodata;
import com.xsis.demo.repository.BiodataRepo;

@Controller
public class BiodataController {
	@Autowired
	private BiodataRepo bioRep;
	
	@RequestMapping(value = "/biodata", method = RequestMethod.GET)
	public String index() {
		
		return "biodata/index";
	}
	
	@RequestMapping(value = "/biodata/create", method = RequestMethod.GET)
	public String create() {
		return "biodata/create";
	}
	
	@RequestMapping(value = "/biodata/edit/{id}", method = RequestMethod.POST)
	public String edit(Model model, @PathVariable(name = "id") Integer id) {
		Biodata item = bioRep.findById(id).orElse(null);
		model.addAttribute("data", item);
		return "biodata/edit";
	}
	
	
	public String delete() {
		return "biodata/delete";
	}
	
	
}
