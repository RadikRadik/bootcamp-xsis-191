package com.xsis.demo;

import java.util.Collections;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootDemo1Application {

	public static void main(String[] args) {
		SpringApplication app= new SpringApplication(SpringBootDemo1Application.class);
		app.setDefaultProperties(Collections.singletonMap("server.port","8083"));
		app.run(args);
	}

}
