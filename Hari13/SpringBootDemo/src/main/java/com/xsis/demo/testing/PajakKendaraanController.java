package com.xsis.demo.testing;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.xsis.demo.model1.PajakKendaraan;

@Controller
public class PajakKendaraanController {
	@RequestMapping("/PajakKendaraan/index")
	public String index() {
		
		return "PajakKendaraan/index";
	}
	
	@RequestMapping("/PajakKendaraan/formIsian")
	public String formIsian() {
		return "PajakKendaraan/formIsian";
	}
	
	@RequestMapping(value= "/PajakKendaraan/save", method=RequestMethod.POST)
	public String save(@ModelAttribute PajakKendaraan item, Model model1) {
		model1.addAttribute("data", item);
		return "PajakKendaraan/save";
	}

	
}

