package com.xsis.demo.testing;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.xsis.demo.model4.DataMahasiswa;

@Controller
public class DataMahasiswaContoller {
	@RequestMapping("/DataMahasiswa/added")
	public String added() {
		return "DataMahasiswa/added";
	}
	
	@RequestMapping(value="/DataMahasiswa/save", method=RequestMethod.POST)
	public String save(@ModelAttribute DataMahasiswa item, Model model4) {
		model4.addAttribute("data",item);
		return "DataMahasiswa/save";
	}
}
