package com.xsis.demo.testing;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.xsis.demo.model2.Bioskop;

@Controller
public class BioskopController {
	@RequestMapping("/Bioskop/mesenTiket")
	public String mesenTiket() {
		return "Bioskop/mesenTiket";
	}
	
	@RequestMapping(value="/Bioskop/save", method=RequestMethod.POST)
	public String save(@ModelAttribute Bioskop item, Model model2) {
		model2.addAttribute("data", item);
		return "Bioskop/save";
	}
}
