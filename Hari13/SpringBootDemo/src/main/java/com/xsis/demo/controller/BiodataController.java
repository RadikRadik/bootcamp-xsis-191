package com.xsis.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.xsis.demo.model.Biodata;
import com.xsis.demo.repository.BiodataRepo;


@Controller
public class BiodataController {
	
	@Autowired
	private BiodataRepo repo;
	
	@RequestMapping("/biodata/index")
	public String index(Model model) {
		
		List<Biodata> data= repo.findAll();
		model.addAttribute("listdata", data);
		return "biodata/index";
	}
	
	@RequestMapping("/biodata/add")
	public String add() {
		return "biodata/add";
	}
	
	@RequestMapping(value="/biodata/save", method=RequestMethod.POST)
	public String save(@ModelAttribute Biodata item) {	
		//model.addAttribute("data", item);item
		repo.save(item);
		return "redirect:/biodata/index";
	}
}
