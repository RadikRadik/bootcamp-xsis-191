package com.xsis.demo.model3;

public class RegistrasiTimeJones {
	private int id;
	private String nama;
	private String jk;
	private String telp;
	private String tipeMember;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getNama() {
		return nama;
	}
	public void setNama(String nama) {
		this.nama = nama;
	}
	public String getJk() {
		return jk;
	}
	public void setJk(String jk) {
		this.jk = jk;
	}
	public String getTelp() {
		return telp;
	}
	public void setTelp(String telp) {
		this.telp = telp;
	}
	public String getTipeMember() {
		return tipeMember;
	}
	public void setTipeMember(String tipeMember) {
		this.tipeMember = tipeMember;
	}
	
	
}
