package com.xsis.demo.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="biodata")
public class Biodata {
	private int id;
	private String nama;
	private String alamat;
	private String jk;
	private String telp;
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="id", nullable=false)
	public int getId() {
		return id;
	}
	@Column(name="nama", nullable=false)
	public String getNama() {
		return nama;
	}
	
	@Column(name="alamat", nullable=false)
	public String getAlamat() {
		return alamat;
	}
	@Column(name="jk", nullable=false)
	public String getJk() {
		return jk;
	}
	@Column(name="telp", nullable=false)
	public String getTelp() {
		return telp;
	}
	
	public void setId(int id) {
		this.id=id;
	}
	
	public void setNama(String nama) {
		this.nama=nama;
	}
	
	public void setAlamat(String alamat) {
		this.alamat=alamat;
	}
	
	public void setJk(String jk) {
		this.jk=jk;
	}
	
	public void setTelp(String telp) {
		this.telp=telp;	
	}
}
