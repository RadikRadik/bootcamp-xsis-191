package com.xsis.demo.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;


import com.xsis.demo.model.Employee;

@Controller
public class EmployeeController {
	@RequestMapping("/employee/add")
	public String add() {
		return "employee/add";
	}
	
	@RequestMapping(value= "/employee/save", method=RequestMethod.POST)
	public String save(@ModelAttribute Employee item, Model model) {
		model.addAttribute("data", item);
		return "employee/save";
	}
}
