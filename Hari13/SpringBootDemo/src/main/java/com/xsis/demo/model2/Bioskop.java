package com.xsis.demo.model2;

public class Bioskop {
	private String nama;
	private String film;
	private int jmlTix;
	public String getNama() {
		return nama;
	}
	public void setNama(String nama) {
		this.nama = nama;
	}
	public String getFilm() {
		return film;
	}
	public void setFilm(String film) {
		this.film = film;
	}
	public int getJmlTix() {
		return jmlTix;
	}
	public void setJmlTix(int jmlTix) {
		this.jmlTix = jmlTix;
	}

	
}
