package com.xsis.demo.testing;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.xsis.demo.model3.RegistrasiTimeJones;

@Controller
public class RegistrasiTimeJonesContoller {
	@RequestMapping("/RegistrasiTimeJones/add")
	public String add() {
		return "RegistrasiTimeJones/add";
	}
	
	@RequestMapping(value="/RegistrasiTimeJones/save", method=RequestMethod.POST)
	public String save(@ModelAttribute RegistrasiTimeJones item, Model model3) {
		model3.addAttribute("data", item);
		return "RegistrasiTimeJones/save";
	}
}
