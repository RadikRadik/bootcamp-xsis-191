package com.xsis.demo.model1;

public class PajakKendaraan {
	private String ktp;
	private String nama;
	private String merek;
	private String model;
	private String noPol;
	private String warna;
	private int mesin;
	public String getKtp() {
		return ktp;
	}
	public void setKtp(String ktp) {
		this.ktp = ktp;
	}
	public String getNama() {
		return nama;
	}
	public void setNama(String nama) {
		this.nama = nama;
	}
	public String getMerek() {
		return merek;
	}
	public void setMerek(String merek) {
		this.merek = merek;
	}
	public String getModel() {
		return model;
	}
	public void setModel(String model) {
		this.model = model;
	}
	public String getNoPol() {
		return noPol;
	}
	public void setNoPol(String noPol) {
		this.noPol = noPol;
	}
	public String getWarna() {
		return warna;
	}
	public void setWarna(String warna) {
		this.warna = warna;
	}
	public int getMesin() {
		return mesin;
	}
	public void setMesin(int mesin) {
		this.mesin = mesin;
	}
	
	
}
