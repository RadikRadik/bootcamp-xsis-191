package com.xsis.demo;

import org.springframework.boot.web.server.ConfigurableWebServerFactory;
import org.springframework.boot.web.server.WebServerFactoryCustomizer;
import org.springframework.stereotype.Component;

//import net.bytebuddy.implementation.Implementation;
@Component
public class ServerPortCostumizer implements WebServerFactoryCustomizer<ConfigurableWebServerFactory>{
	@Override
	public void customize (ConfigurableWebServerFactory factory){
		factory.setPort(8084);
	}
}
