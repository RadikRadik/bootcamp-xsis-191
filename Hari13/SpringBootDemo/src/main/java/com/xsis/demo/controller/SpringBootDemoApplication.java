package com.xsis.demo.controller;

import java.util.Collections;

//import javax.swing.Spring;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootDemoApplication {
	public static void main(String[] args) {
		SpringApplication app= new SpringApplication(SpringBootDemoApplication.class);
		app.setDefaultProperties(Collections.singletonMap("server.port","8082"));
		app.run(args);
	}
}
