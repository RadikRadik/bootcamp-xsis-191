package com.example.demo.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.TableGenerator;

import com.fasterxml.jackson.annotation.JsonBackReference;

@Entity
@Table(name = "jurusan")
public class JurusanModel {

	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "jurusan_seq")
	@TableGenerator(name = "jurusan_seq", table = "tbl_sequence", pkColumnName = "seq_id",
	valueColumnName = "seq_value", initialValue = 0, allocationSize = 1)
	private Integer id;
	
	@Column(name = "kd_jurusan", nullable = false, length=10)
	private String kdJurusan;
	
	@Column(name = "nm_jurusan", nullable = false, length=150)
	private String nmJurusan;
	
	//@Column(name = "KdFakultas", nullable = false, length=10)
	//private String KdFakultas;
	
	@Column(name = "Fakultas_id", nullable = false, updatable = false, insertable = false)
	private Integer fakultasId;
	
	@JsonBackReference
	@ManyToOne
	@JoinColumn(name = "fakultas_id", foreignKey = @ForeignKey(name = "fk_fakultas"))
	private FakultasModel fakultas;

	
	public Integer getFakultasId() {
		return fakultasId;
	}

	public void setFakultasId(Integer fakultasId) {
		this.fakultasId = fakultasId;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getKdJurusan() {
		return kdJurusan;
	}

	public void setKdJurusan(String kdJurusan) {
		this.kdJurusan = kdJurusan;
	}

	public String getNmJurusan() {
		return nmJurusan;
	}

	public void setNmJurusan(String nmJurusan) {
		this.nmJurusan = nmJurusan;
	}

	public FakultasModel getFakultas() {
		return fakultas;
	}

	public void setFakultas(FakultasModel fakultas) {
		this.fakultas = fakultas;
	}

	

	
}
