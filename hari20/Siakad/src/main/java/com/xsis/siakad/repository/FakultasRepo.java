package com.xsis.siakad.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.xsis.siakad.model.FakultasModel;

@Repository
public interface FakultasRepo extends JpaRepository<FakultasModel, Integer>{

}
