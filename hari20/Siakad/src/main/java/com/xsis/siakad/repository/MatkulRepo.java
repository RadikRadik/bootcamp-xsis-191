package com.xsis.siakad.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.xsis.siakad.model.MatkulModel;

@Repository
public interface MatkulRepo extends JpaRepository<MatkulModel, Integer> {

}
