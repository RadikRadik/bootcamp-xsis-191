package com.xsis.siakad.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.xsis.siakad.model.FakultasModel;
import com.xsis.siakad.repository.FakultasRepo;

@Controller
public class FakultasController {
	
	@Autowired
	private FakultasRepo repo;
	
	@RequestMapping(value = "/fakultas", method = RequestMethod.GET)
	public String index(Model model) {
		List<FakultasModel> data = repo.findAll();
		model.addAttribute("listdata",data);
		return "fakultas/index";
	}
	
	@RequestMapping(value = "/fakultas/add")
	public String add() {
		return "fakultas/add";
	}
	
	@RequestMapping(value = "/fakultas/save", method = RequestMethod.POST)
	public String save(@ModelAttribute FakultasModel item) {
		repo.save(item);
		return"redirect:/fakultas/index";
	}
	
	@RequestMapping(value = "/fakultas/edit/{id}")
	public String edit(Model model, @PathVariable(name = "id") Integer id) {
		FakultasModel item = repo.findById(id).orElse(null);
		model.addAttribute("data",item);
		return "fakultas/edit";
	}
	
	@RequestMapping(value = "/fakultas/delete/{id}")
	public String delete(@PathVariable(name="id") Integer id) {
		FakultasModel item = repo.findById(id).orElse(null);
		if(item!=null) {
			repo.delete(item);
		}
		
		return "redirect:/fakultas/index";
		
	}
	
	@RequestMapping(value = "/fakultas/list", method = RequestMethod.GET)
	public String tampilist(Model model) {
		List<FakultasModel> data= repo.findAll();
		model.addAttribute("listdata1", data);
		return "fakultas/list";
	}
	
}
