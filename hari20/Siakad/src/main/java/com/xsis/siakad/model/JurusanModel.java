package com.xsis.siakad.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.TableGenerator;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import lombok.Data;

@Entity
@Table(name = "jurusan")
@Data
public class JurusanModel {

	@Id
	@Column(name="id")
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "jurusan_seq")
	@TableGenerator(name = "jurusan_seq", table = "tbl_sequence", pkColumnName = "seq_id",
	valueColumnName = "seq_value", initialValue = 0, allocationSize = 1)
	private Integer id;
	
	@Column(name="kd_jurusan", nullable = false, length=10)
	private String kode;
	
	@Column(name="nm_jurusan", nullable = false, length=150)
	private String nama;
	
	@Column(name = "fakultas_id", nullable = false)
	private Integer fakultasId;
	
	@JsonBackReference
	@ManyToOne
	@JoinColumn(name = "fakultas_id", foreignKey = @ForeignKey(name = "fk_fakultas"), updatable = false, insertable = false)
	private FakultasModel fakultas;
	
	@JsonManagedReference
	@OneToMany(mappedBy = "jurusan", cascade = CascadeType.ALL)
	private List<MatkulModel> listmatkul= new ArrayList<MatkulModel>();
	
	@JsonManagedReference
	@OneToMany(mappedBy = "jurusan", cascade = CascadeType.ALL)
	private List<MahasiswaModel> listmahasiswa = new ArrayList<MahasiswaModel>();
}
