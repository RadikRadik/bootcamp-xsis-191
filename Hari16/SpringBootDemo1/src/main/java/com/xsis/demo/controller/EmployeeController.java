package com.xsis.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.xsis.demo.model.Employee;
import com.xsis.demo.repository.EmployeeRepo;

@Controller
public class EmployeeController {

	@Autowired
	private EmployeeRepo Repo1;
	
	@RequestMapping("/employee/index")
	public String index(Model model) {
		List<Employee> data= Repo1.findAll();
		model.addAttribute("listdata", data);
		return "employee/index";
	}
	
	@RequestMapping("/employee/add")
	public String add() {
		return "employee/add";
	}
	
	@RequestMapping(value="/employee/save", method=RequestMethod.POST)
	public String save(@ModelAttribute Employee item) {
		Repo1.save(item);
		return "redirect:/employee";
	}
	
	@RequestMapping(value="/employee/delete/{id}")
	public String edit(Model model,@PathVariable(name="id") Long id) {
		Employee item = Repo1.findById(id).orElse(null);
		model.addAttribute("data", item);
		return "employee/edit";
	}
	
	@RequestMapping(value = "/employee/delete/{id}")
	public String hapus(@PathVariable(name="id")Long id) {
		Employee item = Repo1.findById(id).orElse(null);
		if(item!=null) {
			Repo1.delete(item);
		}return "redirect:/employee";
	}
	
}
